import MapSkeleton from './MapSkeleton';
import InfoColumnSkeleton from './InfoColumnSkeleton';
import './App.css';

function App() {
  return (
    <div className="App">
      <InfoColumnSkeleton />
      <MapSkeleton />
    </div>
  );
}

export default App;
