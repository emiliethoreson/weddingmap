import React from 'react';
import LocationsArray from './assets/LocationsArray.json';
import activitypin from './assets/activity.png';
import restaurantpin from './assets/restaurant.png';
import brewerypin from './assets/brewery.png';
import venuepin from './assets/venue.png';
import viewspin from './assets/views.png';
import hotelpin from './assets/hotel.png';
import { Loader } from '@googlemaps/js-api-loader';

function MapSkeleton() {
    const loader = new Loader({
        apiKey: "AIzaSyDctOimoIjSx-z2xYyytVS0TlQvWmNxouE",
        version: "weekly",
        libraries: ["places"],
        mapTypeId: 'roadmap'
      });
      
    const mapOptions = {
        center: {
            lat: 47.6359500,
            lng: -122.3616311
        },
        zoom: 13,
        mapId: "3fc0c4bfff6b3b7"
    };


    loader
    .importLibrary('maps')
    .then(async ({Map}) => {
        const map = new Map(document.getElementById("map"), mapOptions);
        const {Marker} = await loader.importLibrary('marker');
        LocationsArray.map(loc => {
            let infowindow = new window.google.maps.InfoWindow({
                content: '<div><h6>' + loc.name + '</h6><div>' + loc.address + '</div><div>' + loc.notes + '</div></div>'
            });
            let png;
            switch(loc.type) {
                case 'activity': png = activitypin; break;
                case 'restaurant': png = restaurantpin; break;
                case 'brewery': png = brewerypin; break;
                case 'views': png = viewspin; break;
                case 'hotel': png = hotelpin; break;
                case 'venue': png = venuepin; break;
                default: png = activitypin;
            }
            const marker = new Marker({
                map,
                position: {lat: loc.lat, lng: loc.lng},
                icon: png,
                title: loc.name
            })
            marker.addListener("click", () => {
                infowindow.open({
                    anchor: marker,
                    map
                })
            })
            map.addListener("click", function(e) {
                infowindow.close();
            })
            return marker;
        })
    })
    .catch((e) => {
        console.log('MAP LOADING ERROR:', e);
    });


    return (<div id='map'></div>)
}

export default MapSkeleton;

