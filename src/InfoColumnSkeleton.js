import React from 'react';
import './InfoColumnSkeleton.css';
import activity from './assets/activity.png';
import restaurant from './assets/restaurant.png';
import brewery from './assets/brewery.png';
import views from './assets/views.png';
import hotel from './assets/hotel.png';
import venue from './assets/venue.png';

function InfoColumnSkeleton() {
  return (
    <div className="info-column">
        <h1>Welcome to Seattle!<br />¡Bienvenidos a Seattle! </h1>
        <h5>Hannah, Alex, Jupiter, and Freya have compiled a list of their favorite things in the city. 
          Click a pin on the map to get info, discounts and more. 
          We recommend you download the Transit GO app to help you get around.<br /><br />
          Hannah, Alex, Júpiter, y Freya han compilado una lista de sus cosas favoritas en la ciudad. 
          Haga clic en un marcador en el mapa para obtener información, cupones y más. 
          Te recomendamos descargar la aplicación Transit GO para ayudarte a moverte.</h5>
        <div className='color-key'>
          <div>
            <div className='key-row'><img className='key-pin' src={activity} alt="activity-icon" />
            <span> = Activity<br />Actividad</span></div>
            <div className='key-row'><img className='key-pin' src={restaurant} alt="restaurant-icon" />
            <span> = Restaurant<br />Restaurante</span></div>
            <div className='key-row'><img className='key-pin' src={brewery} alt="brewery-icon" />
            <span> = Brewery<br />Cervecería</span></div>
            </div>
            <div>
            <div className='key-row'><img className='key-pin' src={views} alt="views-icon" />
            <span> = View<br />Vista</span></div>
            <div className='key-row'><img className='key-pin' src={hotel} alt="hotel-icon" />
            <span> = Hotel</span></div>
            <div className='key-row'><img className='key-pin' src={venue} alt="venue-icon" />
            <span> = Venue<br />Evento</span></div>
            </div>
        </div>
    </div>
  );
}

export default InfoColumnSkeleton;
